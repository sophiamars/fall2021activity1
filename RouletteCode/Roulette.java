import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        RouletteWheel game = new RouletteWheel();

        System.out.println("Would you like to make a bet? (y/n)");
        char playagain = input.next().charAt(0);

        while (playagain == 'y') {
            System.out.println("Would you like to bet by number, position, even/odd, or colour? (n/p/e/c)");
            char betting = input.next().charAt(0);

            if (betting == 'n') {
                System.out.println("What number would you like to bet on? (0-36)");
                int bet = input.nextInt();

                System.out.println("What amount would you like to bet?");
                int betAmount = input.nextInt();

                game.spin();

                System.out.println("The winning number was "+game.getValue()+"!");
                if (game.getValue() == bet) {
                    System.out.println("You win "+betAmount*35+"$ !");
                }
                else {
                    System.out.println("You lose "+betAmount+"$ :(");
                }
            }
            
            if (betting == 'p') {
                System.out.println("Would you like to bet on low or high? (l/h)");
                char bet = input.next().charAt(0);

                System.out.println("What amount would you like to bet?");
                int betAmount = input.nextInt();

                game.spin();

                if (game.isLow() == true) {
                    System.out.println("The winning position was low!");
                    if (bet == 'l') {
                        System.out.println("You win "+betAmount+"$ !");
                    }
                    else {
                        System.out.println("You lose "+betAmount+"$ :(");
                    }
                }
                else if (game.isHigh() == true) {
                    System.out.println("The winning position was high!");
                    if (bet == 'h') {
                        System.out.println("You win "+betAmount+"$ !");
                    }
                    else {
                        System.out.println("You lose "+betAmount+"$ :(");
                    }
                }
            }

            if (betting == 'e') {
                System.out.println("Would you like to bet on even or odd? (e/o)");
                char bet = input.next().charAt(0);

                System.out.println("What amount would you like to bet?");
                int betAmount = input.nextInt();

                game.spin();

                if (game.isEven() == true) {
                    System.out.println("The winning position was even!");
                    if (bet == 'e') {
                        System.out.println("You win "+betAmount+"$ !");
                    }
                    else {
                        System.out.println("You lose "+betAmount+"$ :(");
                    }
                }
                else if (game.isOdd() == true) {
                    System.out.println("The winning position was odd!");
                    if (bet == 'o') {
                        System.out.println("You win "+betAmount+"$ !");
                    }
                    else {
                        System.out.println("You lose "+betAmount+"$ :(");
                    }
                }
            }

            if (betting == 'c') {
                System.out.println("Would you like to bet on red or black? (r/b)");
                char bet = input.next().charAt(0);

                System.out.println("What amount would you like to bet?");
                int betAmount = input.nextInt();

                game.spin();

                if (game.isRed() == true) {
                    System.out.println("The winning colour was red!");
                    if (bet == 'r') {
                        System.out.println("You win "+betAmount+"$ !");
                    }
                    else {
                        System.out.println("You lose "+betAmount+"$ :(");
                    }
                }
                else if (game.isBlack() == true) {
                    System.out.println("The winning colour was black!");
                    if (bet == 'b') {
                        System.out.println("You win "+betAmount+"$ !");
                    }
                    else {
                        System.out.println("You lose "+betAmount+"$ :(");
                    }
                }
            }
        
            System.out.println("Would you like to make a bet? (y/n)");
            playagain = input.next().charAt(0);
        }
    }
}
