import  java.util.Random;

public class RouletteWheel {
    Random random = new Random();
    private int number = 0;

    public void spin() {
        this.number = random.nextInt(37);
    }

    public int getValue() {
        return this.number;
    }

    public boolean isLow() {
        if (this.number >= 1 && this.number <= 18) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isHigh() {
        if (this.number >= 19 && this.number <= 36) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isEven() {
        if (this.number%2 == 0 && this.number != 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isOdd() {
        if (this.number%2 != 0 || this.number != 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isRed() {
        if ((this.number >= 1 && this.number <= 10) || 
            (this.number >= 19 && this.number <= 28)) {
            if (this.number%2 != 0 || this.number != 0) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (this.number%2 == 0 && this.number != 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public boolean isBlack() {
        if (this.isRed() == true) {
            return false;
        }
        else {
            return true;
        }
    }
}